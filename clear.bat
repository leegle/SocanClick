@echo Off

::递归删除不要的文件
del /s /a *.suo *.ncb *.user *.pdb *.netmodule *.aps *.sdf *.ilk *.vshost.exe.config *.vshost.exe *.manifest 2>nul

::递归删除四个不需要的目录
FOR /R . %%d IN (.) DO rd /s /q "%%d\Debug" 2>nul
FOR /R . %%d IN (.) DO rd /s /q "%%d\Release" 2>nul
FOR /R . %%d IN (.) DO rd /s /q "%%d\Bin" 2>nul
FOR /R . %%d IN (.) DO rd /s /q "%%d\Obj" 2>nul

::如果Properties是空的，则删除
FOR /R . %%d in (.) do rd /q "%%d\Properties" 2>nul
echo 清理完成
pause