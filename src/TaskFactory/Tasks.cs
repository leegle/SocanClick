﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskFactory
{
    public class Tasks
    {
        public static TaskItems.TaskItem CreateTaskItem(string task)
        {
            if (task.StartsWith(TaskItems.Click.TaskDescn))
            {
                string p = task.Replace(TaskItems.Click.TaskDescn, "");
                string[] ps = p.Split('.');
                int x = Convert.ToInt32(ps[0]);
                int y = Convert.ToInt32(ps[1]);
                return new TaskItems.Click(x, y);
            }

            if (task.StartsWith(TaskItems.RightClick.TaskDescn))
            {
                string p = task.Replace(TaskItems.RightClick.TaskDescn, "");
                string[] ps = p.Split('.');
                int x = Convert.ToInt32(ps[0]);
                int y = Convert.ToInt32(ps[1]);
                return new TaskItems.RightClick(x, y);
            }

            if (task.StartsWith(TaskItems.Connect.TaskDescn))
            {
                return new TaskItems.Connect();
            }

            if (task.StartsWith(TaskItems.Disconnect.TaskDescn))
            {
                return new TaskItems.Disconnect();
            }

            if (task.StartsWith(TaskItems.Sleep.TaskDescn))
            {
                return new TaskItems.Sleep(Convert.ToInt32(task.Replace(TaskItems.Sleep.TaskDescn, "")));
            }

            if (task.StartsWith(TaskItems.OpenUrl.TaskDescn))
            {
                return new TaskItems.OpenUrl(task.Replace(TaskItems.OpenUrl.TaskDescn, ""));
            }

            if (task.StartsWith(TaskItems.ClearCache.TaskDescn))
            {
                return new TaskItems.ClearCache();
            }

            return null;
        }
    }
}
