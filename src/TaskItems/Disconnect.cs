﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskItems
{
    public class Disconnect:TaskItem
    {
        public static string TaskDescn
        {
            get { return "断开连接"; }
        }

        public override void Do()
        {
            TaskHelper.RASDisplay r = new TaskHelper.RASDisplay();
            r.Disconnect();
        }

        public override string ToString()
        {
            return TaskDescn;
        }
    }
}
