﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskItems
{
    public class ClearCache : TaskItem
    {
        public static string TaskDescn
        {
            get { return "清空缓存"; }
        }

        public override void Do()
        {
            TaskHelper.DeleteCache.ClearCache();
        }

        public override string ToString()
        {
            return TaskDescn;
        }
    }
}
