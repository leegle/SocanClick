﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace TaskItems
{
    public class OpenUrl : TaskItem
    {
        public string Url { get; set; }

        public OpenUrl(string url)
        {
            this.Url = url;
        }

        public static string TaskDescn
        {
            get { return "打开"; }
        }

        public override void Do()
        {
            Process.Start(this.Url);
        }

        public override string ToString()
        {
            return TaskDescn + Url;
        }
    }
}
