﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace TaskItems
{
    public class Connect : TaskItem
    {
        public static string TaskDescn
        {
            get { return "连接"; }
        }

        public override void Do()
        {
            TaskHelper.RASDisplay r = new TaskHelper.RASDisplay();
            r.Connect("adsl");

            if (r.IsConnected)
            {
                try
                {
                    IpAddressSearchWebService.IpAddressSearchWebService ipsvr = new IpAddressSearchWebService.IpAddressSearchWebService();
                    string[] result = ipsvr.getGeoIPContext();
                    StringBuilder sb = new StringBuilder();
                    if (!File.Exists("ip.txt"))
                        File.Create("ip.txt");
                    using (StreamReader sr = new StreamReader("ip.txt"))
                    {
                        sb.Append(sr.ReadToEnd());
                    }

                    sb.AppendLine(result[0]);

                    using (StreamWriter sw = new StreamWriter("ip.txt"))
                    {
                        sw.Write(sb.ToString());
                    }
                }
                catch (Exception)
                {
                    // 失败不做处理
                }
            }
        }

        public override string ToString()
        {
            return TaskDescn;
        }
    }
}
