﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskItems
{
    public abstract class TaskItem
    {
        public abstract void Do();
    }
}
