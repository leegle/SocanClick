﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace TaskItems
{
    public class Sleep : TaskItem
    {
        public int Time { get; set; }

        public Sleep(int time)
        {
            this.Time = time;
        }

        public static string TaskDescn
        {
            get { return "休眠"; }
        }

        public override void Do()
        {
            Thread.Sleep(this.Time);
        }

        public override string ToString()
        {
            return TaskDescn + Time.ToString();
        }
    }
}
