﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace TaskItems
{
    public class Click : TaskItem
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Click(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        public static string TaskDescn
        {
            get { return "点击"; }
        }

        public override void Do()
        {
            Cursor.Position = new Point(X, Y);
            TaskHelper.MouseEvent.mouse_event(TaskHelper.MouseEvent.MOUSEEVENTF_LEFTDOWN | TaskHelper.MouseEvent.MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
        }

        public override string ToString()
        {
            return TaskDescn + X + "." + Y;
        }
    }
}
