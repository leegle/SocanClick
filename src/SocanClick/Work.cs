﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SocanClick
{
    public class Work
    {
        public List<TaskItems.TaskItem> tasks;

        private int _loopCount = 10;
        public int LoopCount
        {
            get
            {
                return _loopCount;
            }
            set
            {
                if (value <= 0)
                    return;

                _loopCount = value;
            }
        }

        public Work()
        {
            tasks = new List<TaskItems.TaskItem>();
        }

        public void AddTask(TaskItems.TaskItem taskItem)
        {
            tasks.Add(taskItem);
        }
    }
}
