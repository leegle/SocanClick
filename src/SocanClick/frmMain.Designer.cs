﻿namespace SocanClick
{
    partial class frmMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnConnect = new System.Windows.Forms.Button();
            this.txtX = new System.Windows.Forms.NumericUpDown();
            this.txtY = new System.Windows.Forms.NumericUpDown();
            this.btnDisconnect = new System.Windows.Forms.Button();
            this.btnClick = new System.Windows.Forms.Button();
            this.labSleep = new System.Windows.Forms.Label();
            this.btnOpen = new System.Windows.Forms.Button();
            this.txtURL = new System.Windows.Forms.TextBox();
            this.lstTask = new System.Windows.Forms.ListBox();
            this.btnSleep = new System.Windows.Forms.Button();
            this.txtSleep = new System.Windows.Forms.NumericUpDown();
            this.btnStart = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnTaskUp = new System.Windows.Forms.Button();
            this.btnTaskDown = new System.Windows.Forms.Button();
            this.btnTaskBottom = new System.Windows.Forms.Button();
            this.btnTaskTop = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtNumber = new System.Windows.Forms.NumericUpDown();
            this.btnTasksClear = new System.Windows.Forms.Button();
            this.picCursor = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.btnClearCache = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnRightClick = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.mnuTask = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuTaskSaveAs = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuTaskLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.帮助ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tsslStatus = new System.Windows.Forms.ToolStripStatusLabel();
            ((System.ComponentModel.ISupportInitialize)(this.txtX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSleep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCursor)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(16, 23);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(104, 23);
            this.btnConnect.TabIndex = 0;
            this.btnConnect.Text = "连接";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // txtX
            // 
            this.txtX.Location = new System.Drawing.Point(69, 24);
            this.txtX.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.txtX.Name = "txtX";
            this.txtX.Size = new System.Drawing.Size(53, 21);
            this.txtX.TabIndex = 1;
            this.txtX.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // txtY
            // 
            this.txtY.Location = new System.Drawing.Point(136, 25);
            this.txtY.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.txtY.Name = "txtY";
            this.txtY.Size = new System.Drawing.Size(53, 21);
            this.txtY.TabIndex = 1;
            this.txtY.Value = new decimal(new int[] {
            200,
            0,
            0,
            0});
            // 
            // btnDisconnect
            // 
            this.btnDisconnect.Location = new System.Drawing.Point(127, 23);
            this.btnDisconnect.Name = "btnDisconnect";
            this.btnDisconnect.Size = new System.Drawing.Size(91, 23);
            this.btnDisconnect.TabIndex = 0;
            this.btnDisconnect.Text = "断开";
            this.btnDisconnect.UseVisualStyleBackColor = true;
            this.btnDisconnect.Click += new System.EventHandler(this.btnDisconnect_Click);
            // 
            // btnClick
            // 
            this.btnClick.Location = new System.Drawing.Point(202, 23);
            this.btnClick.Name = "btnClick";
            this.btnClick.Size = new System.Drawing.Size(40, 23);
            this.btnClick.TabIndex = 2;
            this.btnClick.Text = "点击";
            this.btnClick.UseVisualStyleBackColor = true;
            this.btnClick.Click += new System.EventHandler(this.btnClick_Click);
            // 
            // labSleep
            // 
            this.labSleep.AutoSize = true;
            this.labSleep.Location = new System.Drawing.Point(11, 26);
            this.labSleep.Name = "labSleep";
            this.labSleep.Size = new System.Drawing.Size(101, 12);
            this.labSleep.TabIndex = 6;
            this.labSleep.Text = "休眠时间（毫秒）";
            // 
            // btnOpen
            // 
            this.btnOpen.Location = new System.Drawing.Point(253, 22);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(70, 23);
            this.btnOpen.TabIndex = 2;
            this.btnOpen.Text = "打开";
            this.btnOpen.UseVisualStyleBackColor = true;
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // txtURL
            // 
            this.txtURL.Location = new System.Drawing.Point(58, 23);
            this.txtURL.Name = "txtURL";
            this.txtURL.Size = new System.Drawing.Size(184, 21);
            this.txtURL.TabIndex = 7;
            this.txtURL.Text = "http://www.baidu.com";
            // 
            // lstTask
            // 
            this.lstTask.FormattingEnabled = true;
            this.lstTask.ItemHeight = 12;
            this.lstTask.Location = new System.Drawing.Point(18, 25);
            this.lstTask.Name = "lstTask";
            this.lstTask.Size = new System.Drawing.Size(186, 172);
            this.lstTask.TabIndex = 8;
            // 
            // btnSleep
            // 
            this.btnSleep.Location = new System.Drawing.Point(253, 22);
            this.btnSleep.Name = "btnSleep";
            this.btnSleep.Size = new System.Drawing.Size(70, 23);
            this.btnSleep.TabIndex = 9;
            this.btnSleep.Text = "休眠";
            this.btnSleep.UseVisualStyleBackColor = true;
            this.btnSleep.Click += new System.EventHandler(this.btnSleep_Click);
            // 
            // txtSleep
            // 
            this.txtSleep.Location = new System.Drawing.Point(118, 22);
            this.txtSleep.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.txtSleep.Name = "txtSleep";
            this.txtSleep.Size = new System.Drawing.Size(124, 21);
            this.txtSleep.TabIndex = 1;
            this.txtSleep.Value = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            // 
            // btnStart
            // 
            this.btnStart.ForeColor = System.Drawing.Color.Red;
            this.btnStart.Location = new System.Drawing.Point(160, 41);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(64, 23);
            this.btnStart.TabIndex = 9;
            this.btnStart.Text = "开始(&S)";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 6;
            this.label1.Text = "坐标： X";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(124, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(11, 12);
            this.label3.TabIndex = 6;
            this.label3.Text = "Y";
            // 
            // btnTaskUp
            // 
            this.btnTaskUp.Location = new System.Drawing.Point(210, 49);
            this.btnTaskUp.Name = "btnTaskUp";
            this.btnTaskUp.Size = new System.Drawing.Size(57, 23);
            this.btnTaskUp.TabIndex = 10;
            this.btnTaskUp.Text = "上移";
            this.btnTaskUp.UseVisualStyleBackColor = true;
            this.btnTaskUp.Click += new System.EventHandler(this.btnTaskUp_Click);
            // 
            // btnTaskDown
            // 
            this.btnTaskDown.Location = new System.Drawing.Point(210, 74);
            this.btnTaskDown.Name = "btnTaskDown";
            this.btnTaskDown.Size = new System.Drawing.Size(57, 23);
            this.btnTaskDown.TabIndex = 10;
            this.btnTaskDown.Text = "下移";
            this.btnTaskDown.UseVisualStyleBackColor = true;
            this.btnTaskDown.Click += new System.EventHandler(this.btnTaskDown_Click);
            // 
            // btnTaskBottom
            // 
            this.btnTaskBottom.Location = new System.Drawing.Point(210, 99);
            this.btnTaskBottom.Name = "btnTaskBottom";
            this.btnTaskBottom.Size = new System.Drawing.Size(57, 23);
            this.btnTaskBottom.TabIndex = 10;
            this.btnTaskBottom.Text = "置底";
            this.btnTaskBottom.UseVisualStyleBackColor = true;
            this.btnTaskBottom.Click += new System.EventHandler(this.btnTaskBottom_Click);
            // 
            // btnTaskTop
            // 
            this.btnTaskTop.Location = new System.Drawing.Point(210, 25);
            this.btnTaskTop.Name = "btnTaskTop";
            this.btnTaskTop.Size = new System.Drawing.Size(57, 23);
            this.btnTaskTop.TabIndex = 10;
            this.btnTaskTop.Text = "置顶";
            this.btnTaskTop.UseVisualStyleBackColor = true;
            this.btnTaskTop.Click += new System.EventHandler(this.btnTaskTop_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 47);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 6;
            this.label4.Text = "循环次数";
            // 
            // txtNumber
            // 
            this.txtNumber.Location = new System.Drawing.Point(77, 43);
            this.txtNumber.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.txtNumber.Name = "txtNumber";
            this.txtNumber.Size = new System.Drawing.Size(67, 21);
            this.txtNumber.TabIndex = 11;
            this.txtNumber.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // btnTasksClear
            // 
            this.btnTasksClear.Location = new System.Drawing.Point(210, 173);
            this.btnTasksClear.Name = "btnTasksClear";
            this.btnTasksClear.Size = new System.Drawing.Size(57, 23);
            this.btnTasksClear.TabIndex = 10;
            this.btnTasksClear.Text = "清空";
            this.btnTasksClear.UseVisualStyleBackColor = true;
            this.btnTasksClear.Click += new System.EventHandler(this.btnTasksClear_Click);
            // 
            // picCursor
            // 
            this.picCursor.Image = global::SocanClick.Properties.Resources.cursor;
            this.picCursor.Location = new System.Drawing.Point(296, 18);
            this.picCursor.Name = "picCursor";
            this.picCursor.Size = new System.Drawing.Size(27, 29);
            this.picCursor.TabIndex = 12;
            this.picCursor.TabStop = false;
            this.picCursor.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picCursor_MouseDown);
            this.picCursor.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picCursor_MouseUp);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 6;
            this.label2.Text = "网址：";
            // 
            // btnClearCache
            // 
            this.btnClearCache.Location = new System.Drawing.Point(227, 23);
            this.btnClearCache.Name = "btnClearCache";
            this.btnClearCache.Size = new System.Drawing.Size(96, 23);
            this.btnClearCache.TabIndex = 0;
            this.btnClearCache.Text = "清空缓存";
            this.btnClearCache.UseVisualStyleBackColor = true;
            this.btnClearCache.Click += new System.EventHandler(this.btnClearCache_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(210, 144);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(57, 23);
            this.btnDelete.TabIndex = 10;
            this.btnDelete.Text = "删除";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.WorkerSupportsCancellation = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            this.backgroundWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker1_ProgressChanged);
            // 
            // btnStop
            // 
            this.btnStop.Enabled = false;
            this.btnStop.ForeColor = System.Drawing.Color.Red;
            this.btnStop.Location = new System.Drawing.Point(230, 42);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(57, 23);
            this.btnStop.TabIndex = 9;
            this.btnStop.Text = "停止(&T)";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnRightClick
            // 
            this.btnRightClick.Location = new System.Drawing.Point(245, 23);
            this.btnRightClick.Name = "btnRightClick";
            this.btnRightClick.Size = new System.Drawing.Size(40, 23);
            this.btnRightClick.TabIndex = 2;
            this.btnRightClick.Text = "右击";
            this.btnRightClick.UseVisualStyleBackColor = true;
            this.btnRightClick.Click += new System.EventHandler(this.btnRightClick_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtY);
            this.groupBox1.Controls.Add(this.picCursor);
            this.groupBox1.Controls.Add(this.txtX);
            this.groupBox1.Controls.Add(this.btnClick);
            this.groupBox1.Controls.Add(this.btnRightClick);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(320, 41);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(337, 55);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "鼠标（请将右边的图片拖到合适的地方）";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtURL);
            this.groupBox2.Controls.Add(this.btnOpen);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(320, 102);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(337, 55);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "打开网站";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtSleep);
            this.groupBox3.Controls.Add(this.labSleep);
            this.groupBox3.Controls.Add(this.btnSleep);
            this.groupBox3.Location = new System.Drawing.Point(320, 166);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(337, 51);
            this.groupBox3.TabIndex = 15;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "休眠（单位：毫秒）";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btnConnect);
            this.groupBox4.Controls.Add(this.btnDisconnect);
            this.groupBox4.Controls.Add(this.btnClearCache);
            this.groupBox4.Location = new System.Drawing.Point(320, 227);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(337, 55);
            this.groupBox4.TabIndex = 16;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "辅助";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuTask,
            this.帮助ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(671, 24);
            this.menuStrip1.TabIndex = 17;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // mnuTask
            // 
            this.mnuTask.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuTaskSaveAs,
            this.mnuTaskLoad});
            this.mnuTask.Name = "mnuTask";
            this.mnuTask.Size = new System.Drawing.Size(41, 20);
            this.mnuTask.Text = "任务";
            // 
            // mnuTaskSaveAs
            // 
            this.mnuTaskSaveAs.Name = "mnuTaskSaveAs";
            this.mnuTaskSaveAs.Size = new System.Drawing.Size(106, 22);
            this.mnuTaskSaveAs.Text = "另存为";
            this.mnuTaskSaveAs.Click += new System.EventHandler(this.mnuTaskSaveAs_Click);
            // 
            // mnuTaskLoad
            // 
            this.mnuTaskLoad.Name = "mnuTaskLoad";
            this.mnuTaskLoad.Size = new System.Drawing.Size(106, 22);
            this.mnuTaskLoad.Text = "载入";
            this.mnuTaskLoad.Click += new System.EventHandler(this.mnuTaskLoad_Click);
            // 
            // 帮助ToolStripMenuItem
            // 
            this.帮助ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuAbout});
            this.帮助ToolStripMenuItem.Name = "帮助ToolStripMenuItem";
            this.帮助ToolStripMenuItem.Size = new System.Drawing.Size(41, 20);
            this.帮助ToolStripMenuItem.Text = "帮助";
            // 
            // mnuAbout
            // 
            this.mnuAbout.Name = "mnuAbout";
            this.mnuAbout.Size = new System.Drawing.Size(152, 22);
            this.mnuAbout.Text = "关于";
            this.mnuAbout.Click += new System.EventHandler(this.mnuAbout_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.lstTask);
            this.groupBox5.Controls.Add(this.btnTaskUp);
            this.groupBox5.Controls.Add(this.btnTaskDown);
            this.groupBox5.Controls.Add(this.btnTaskBottom);
            this.groupBox5.Controls.Add(this.btnTasksClear);
            this.groupBox5.Controls.Add(this.btnDelete);
            this.groupBox5.Controls.Add(this.btnTaskTop);
            this.groupBox5.Location = new System.Drawing.Point(14, 74);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(278, 208);
            this.groupBox5.TabIndex = 18;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "任务";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsslStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 293);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(671, 22);
            this.statusStrip1.TabIndex = 19;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tsslStatus
            // 
            this.tsslStatus.Name = "tsslStatus";
            this.tsslStatus.Size = new System.Drawing.Size(29, 17);
            this.tsslStatus.Text = "就绪";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(671, 315);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txtNumber);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "自动点击器";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmMain_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.txtX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSleep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCursor)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.NumericUpDown txtX;
        private System.Windows.Forms.NumericUpDown txtY;
        private System.Windows.Forms.Button btnDisconnect;
        private System.Windows.Forms.Button btnClick;
        private System.Windows.Forms.Label labSleep;
        private System.Windows.Forms.Button btnOpen;
        private System.Windows.Forms.TextBox txtURL;
        private System.Windows.Forms.ListBox lstTask;
        private System.Windows.Forms.Button btnSleep;
        private System.Windows.Forms.NumericUpDown txtSleep;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnTaskUp;
        private System.Windows.Forms.Button btnTaskDown;
        private System.Windows.Forms.Button btnTaskBottom;
        private System.Windows.Forms.Button btnTaskTop;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown txtNumber;
        private System.Windows.Forms.Button btnTasksClear;
        private System.Windows.Forms.PictureBox picCursor;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnClearCache;
        private System.Windows.Forms.Button btnDelete;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnRightClick;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem mnuTask;
        private System.Windows.Forms.ToolStripMenuItem mnuTaskSaveAs;
        private System.Windows.Forms.ToolStripMenuItem mnuTaskLoad;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tsslStatus;
        private System.Windows.Forms.ToolStripMenuItem 帮助ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mnuAbout;
    }
}

