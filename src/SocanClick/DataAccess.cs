﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace SocanClick
{
    public class DataAccess
    {
        /// <summary>
        /// 载入任务
        /// </summary>
        public static Work LoadTask(string path)
        {
            Work work = new Work();
            if (File.Exists(path))
            {
                using (StreamReader sr = new StreamReader(path))
                {
                    string item;
                    while ((item = sr.ReadLine()) != null)
                        work.AddTask(TaskFactory.Tasks.CreateTaskItem(item));
                }
            }
            return work;
        }

        /// <summary>
        /// 保存任务
        /// </summary>
        public static void SaveTask(Work work, string path)
        {
            using (StreamWriter sw = new StreamWriter(path))
            {
                foreach (TaskItems.TaskItem item in work.tasks)
                {
                    sw.WriteLine(item.ToString());
                }
            }
        }
    }
}
